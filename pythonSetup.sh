#!/bin/bash

# First, ensure the python-devel packahe is installed
sudo zypper install python3-devel geos-devel

# pull down the 1.18.0  numpy release from github
git clone https://github.com/numpy/numpy

# Change directory into the downloaded numpy repo and checkout branch v1.18.0
cd numpy
git checkout v1.18.0
git fetch

# cp over site.cfg.example to site.cfg and enable the mkl section
cp site.cfg.example site.cfg

echo "[mkl]" >> site.cfg
echo "include_dirs = /home/bobby/intel/mkl/include" >> site.cfg 
echo "library_dirs = /home/bobby/intel/mkl/lib/intel64" >> site.cfg
echo "mkl_libs = mkl_def, mkl_intel_lp64, mkl_gnu_thread, mkl_core, mkl_mc3" >> site.cfg
echo "lapack_libs = mkl_def, mkl_intel_lp64, mkl_gnu_thread, mkl_core, mkl_mc3" >> site.cfg

# Configure build  environment variables for building numpy
export LD_LIBRARY_PATH=/home/bobby/intel/mkl/lib/intel64:${LD_LIBRARY_PATH}

export CFLAGS='-fopenmp -O2 -march=native -ftree-vectorize'
export LDFLAGS='-lm -lpthread -lgomp'

# execute the build command
python3 setup.py build 2>&1

# install into the user directory
python3 setup.py install --user 2>&1

# exit the numpy directory
cd ..

# pip dependency pybind11
pip3 install --user pybind11

# now let's focus on scipy
git clone https://github.com/scipy/scipy

cd scipy
git checkout v1.4.1
git fetch

# export an additional environment variable for scipy
export LDFLAGS="${LDFLAGS} -shared"

# execute the build command
python3 setup.py build 2>&1

# install into the user directory
python3 setup.py install --user 2>&1

# leave the directory
cd ..

# now pip install the rest of the useful data science libraries for python3 
pip3 install --user pandas matplotlib seaborn plotly shapely pyshp scikit-learn pystan pyspark findspark


# get cartopy setup for use with pip
export CFLAGS='-DACCEPT_USE_OF_DEPRECATED_PROJ_API_H=1'
pip3 install --user cartopy 


# clean up the git repos 
rm -rf numpy scipy